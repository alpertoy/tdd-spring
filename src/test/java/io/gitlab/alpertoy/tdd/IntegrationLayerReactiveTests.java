package io.gitlab.alpertoy.tdd;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.gitlab.alpertoy.tdd.dtos.TransactionDTO;
import io.gitlab.alpertoy.tdd.entities.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationLayerReactiveTests {

    private ObjectMapper objectMapper;

    @Autowired
    private WebTestClient client;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    @Order(1)
    void testTransfer() throws JsonProcessingException {
        // given
        TransactionDTO dto = new TransactionDTO();
        dto.setFromAccountId(1L);
        dto.setToAccountId(2L);
        dto.setBankId(1L);
        dto.setAmount(new BigDecimal("100"));

        Map<String, Object> response = new HashMap<>();
        response.put("date", LocalDate.now().toString());
        response.put("status", "OK");
        response.put("message", "Money has been transferred successfully!");
        response.put("transaction", dto);

        // when
        client.post().uri("/api/accounts/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(dto)
                .exchange()
                // then
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .consumeWith(resp -> {
                    try {
                        JsonNode json = objectMapper.readTree(resp.getResponseBody());
                        assertEquals("Money has been transferred successfully!", json.path("message").asText());
                        assertEquals(1L, json.path("transaction").path("fromAccountId").asLong());
                        assertEquals(LocalDate.now().toString(), json.path("date").asText());
                        assertEquals("100", json.path("transaction").path("amount").asText());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                })
                .jsonPath("$.message").isNotEmpty()
                .jsonPath("$.message").value(is("Money has been transferred successfully!"))
                .jsonPath("$.message").value(value -> assertEquals("Money has been transferred successfully!", value))
                .jsonPath("$.message").isEqualTo("Money has been transferred successfully!")
                .jsonPath("$.transaction.fromAccountId").isEqualTo(dto.getFromAccountId())
                .jsonPath("$.date").isEqualTo(LocalDate.now().toString())
                .json(objectMapper.writeValueAsString(response));

    }

    @Test
    @Order(2)
    void testFindAccount() throws JsonProcessingException {

        Account account = new Account(1L, "Alper", new BigDecimal("1000"));

        client.get().uri("/api/accounts/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.username").isEqualTo("Alper")
                .jsonPath("$.balance").isEqualTo(1000)
                .json(objectMapper.writeValueAsString(account));
    }

    @Test
    @Order(3)
    void testFindAccount2() {

        client.get().uri("/api/accounts/2").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Account.class)
                .consumeWith(response -> {
                    Account account = response.getResponseBody();
                    assertNotNull(account);
                    assertEquals("John", account.getUsername());
                    assertEquals("2000.00", account.getBalance().toPlainString());
                });
    }

    @Test
    @Order(4)
    void testFindAllAccounts() {
        client.get().uri("/api/accounts").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$[0].username").isEqualTo("Alper")
                .jsonPath("$[0].id").isEqualTo(1)
                .jsonPath("$[0].balance").isEqualTo(1000)
                .jsonPath("$[1].username").isEqualTo("John")
                .jsonPath("$[1].id").isEqualTo(2)
                .jsonPath("$[1].balance").isEqualTo(2000)
                .jsonPath("$").isArray()
                .jsonPath("$").value(hasSize(2));
    }

    @Test
    @Order(5)
    void testFindAllAccounts2 () {
        client.get().uri("/api/accounts").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Account.class)
                .consumeWith(response -> {
                    List<Account> accounts = response.getResponseBody();
                    assertNotNull(accounts);
                    assertEquals(2, accounts.size());
                    assertEquals(1L, accounts.get(0).getId());
                    assertEquals("Alper", accounts.get(0).getUsername());
                    assertEquals(1000, accounts.get(0).getBalance().intValue());
                    assertEquals(2L, accounts.get(1).getId());
                    assertEquals("John", accounts.get(1).getUsername());
                    assertEquals("2000.0", accounts.get(1).getBalance().toPlainString());
                })
                .hasSize(2)
                .value(hasSize(2));
    }

    @Test
    @Order(6)
    void testSaveAccount() {
        // given
        Account account = new Account(null, "Mark", new BigDecimal("3000"));

        // when
        client.post().uri("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(account)
                .exchange()
                // then
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo(4)
                .jsonPath("$.username").isEqualTo("Mark")
                .jsonPath("$.username").value(is("Mark"))
                .jsonPath("$.balance").isEqualTo(3000);
    }

    @Test
    @Order(7)
    void testSaveAccount2() {
        // given
        Account account = new Account(null, "Lilly", new BigDecimal("3500"));

        // when
        client.post().uri("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(account)
                .exchange()
                // then
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Account.class)
                .consumeWith(response -> {
                    Account a = response.getResponseBody();
                    assertNotNull(a);
                    assertEquals(5L, a.getId());
                    assertEquals("Lilly", a.getUsername());
                    assertEquals("3500", a.getBalance().toPlainString());
                });
    }

    @Test
    @Order(8)
    void testDeleteAccount() {
        client.get().uri("/api/accounts").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Account.class)
                .hasSize(4);

        client.delete().uri("/api/accounts/3")
                .exchange()
                .expectStatus().isNoContent()
                .expectBody().isEmpty();

        client.get().uri("/api/accounts").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Account.class)
                .hasSize(4);

        client.get().uri("/api/accounts/3").exchange()
                .expectStatus().isNotFound()
                .expectBody().isEmpty();
    }
}