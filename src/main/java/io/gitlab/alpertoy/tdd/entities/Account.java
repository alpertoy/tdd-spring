package io.gitlab.alpertoy.tdd.entities;

import io.gitlab.alpertoy.tdd.exceptions.InsufficientFundsException;
import jakarta.persistence.*;
import lombok.*;


import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;
    private BigDecimal balance;

    public void withdraw(BigDecimal amount) {
        BigDecimal newBalance = this.balance.subtract(amount);
        if(newBalance.compareTo(BigDecimal.ZERO) < 0){
            throw new InsufficientFundsException("Insufficient funds in the account.");
        }
        this.balance = newBalance;
    }

    public void add(BigDecimal amount) {
        this.balance = balance.add(amount);
    }
}
