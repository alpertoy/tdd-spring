package io.gitlab.alpertoy.tdd.repositories;

import io.gitlab.alpertoy.tdd.entities.Bank;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankRepository extends JpaRepository<Bank, Long> {
}