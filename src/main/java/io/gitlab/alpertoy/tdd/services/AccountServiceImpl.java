package io.gitlab.alpertoy.tdd.services;

import io.gitlab.alpertoy.tdd.entities.Account;
import io.gitlab.alpertoy.tdd.entities.Bank;
import io.gitlab.alpertoy.tdd.repositories.AccountRepository;
import io.gitlab.alpertoy.tdd.repositories.BankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private BankRepository bankRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Account findById(Long id) {
        return accountRepository.findById(id).orElseThrow();
    }

    @Override
    @Transactional
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        accountRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public int getTotalTransfers(Long bankId) {
        Bank bank = bankRepository.findById(bankId).orElseThrow();
        return bank.getTotalTransfers();
    }

    @Override
    @Transactional(readOnly = true)
    public BigDecimal getBalance(Long accountId) {
        Account account = accountRepository.findById(accountId).orElseThrow();
        return account.getBalance();
    }

    @Override
    @Transactional
    public void transfer(Long fromAccountNum, Long toAccountNum, BigDecimal amount, Long bankId) {
        Account fromAccount = accountRepository.findById(fromAccountNum).orElseThrow();
        fromAccount.withdraw(amount);
        accountRepository.save(fromAccount);

        Account toAccount = accountRepository.findById(fromAccountNum).orElseThrow();
        toAccount.add(amount);
    }
}
